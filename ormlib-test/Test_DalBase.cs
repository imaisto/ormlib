﻿using NUnit.Framework;
using static orm_lib.Base.Attributes;
using orm_lib.Base;
using orm_lib.Connector;
using System;
using orm_lib.Command;
using System.Collections.Generic;

namespace orm_lib_test
{
    class Test_DalBase
    {


        [Table("Tabella")]
        public class TestORM : DalBase
        {
            [IDKey("Id")]
            public long id { get; set; }

            [Field("BigintField", true)]
            public long bigInt { get; set; }

            [Field("FloatField", true)]
            public float floatField { get; set; }
            [Field("IntField", true)]
            public int intField { get; set; }
            [Field("VarcharField", true)]
            public string varcharField { get; set; }
            [Field("DatetimeField", true)]
            public DateTime datetimeField { get; set; }
            [Field("TimestampField", true)]
            public DateTime timestampField { get; set; }
            [Field("BoolField", true)]
            public bool booleanField { get; set; }

        }



        [SetUp]
        public void Setup()
        {

        }
        //funzione privata che fa il truncate di tutte le tabelle del db
        [Test]
        public void T01_Load()
        {
            ConnectorSqlServer con = new ConnectorSqlServer();
            con.Open();

            TestORM o = new TestORM();
            o.Connection = con;
            o.varcharField = "Test1";
            o.Insert();
            o.Load(new DataFieldCondition[] { new DataFieldCondition("VarcharField", o.varcharField) });


        }

    }
}
