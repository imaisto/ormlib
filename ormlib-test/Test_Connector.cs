﻿using NUnit.Framework;
using static orm_lib.Base.Attributes;
using orm_lib.Base;
using orm_lib.Connector;
using System;
using orm_lib.Command;
using System.Collections.Generic;


namespace orm_lib_test
{
    class Test_Connector
    {

        [SetUp]
        public void Setup()
        {

        }
        //funzione privata che fa il truncate di tutte le tabelle del db
        [Test]
        public void T01_openconnection()
        {
            
            ConnectorSqlServer con = new ConnectorSqlServer();

            try
            {
                con.Open();
            }
            catch (Exception e)
            {
                Assert.IsTrue(e.Message == "Connessione non riuscita");
            }


        }
        

        [Test]
        public void T01_closeconnection()
        {

            ConnectorSqlServer con = new ConnectorSqlServer();
            try { 
                con.Close();
            }
            catch(Exception e)
            {
                Assert.IsTrue(e.Message=="La connessione non può essere chiusa");
            }
        }
        [Test]
        public void T01_Execute()
        {
            ConnectorSqlServer con = new ConnectorSqlServer();
            con.Execute("insert into Tabella (VarcharField,IntField) values ('Wonder Woman',40)");

        }
        [Test]
        public void T01_Execute_with_wrong_null_string_or_select()
        {
            ConnectorSqlServer con = new ConnectorSqlServer();
            
            try
            {
                con.Execute("select");
            }
            catch(Exception e)
            {
                Assert.IsTrue(e.Message == "La richiesta non è valida");
            }
        }
        [Test] 
        public void T01_Execute_with_wrong_string()
        {   //perche non vuoi andare
            ConnectorSqlServer con = new ConnectorSqlServer();
            try
            {
                con.Execute("Just a string");
            }
            catch(Exception e)
            {
                Assert.IsTrue(e.Message == "La richiesta non è valida");
            }
        }




    }
}
