using NUnit.Framework;
using orm_lib.Base;
using orm_lib.Command;
using orm_lib.Connector;
using System;
using System.Collections.Generic;
using static orm_lib.Base.Attributes;

namespace orm_lib_test
{
    public class Test_CommandDALParserSqlServer
    {

        [Table("Tabella")]
        public class TestORM : DalBase
        {
            [IDKey("Id")]
            public long id { get; set; }

            [Field("BigintField", true)]
            public long bigInt { get; set; }

            [Field("FloatField", true)]
            public float floatField { get; set; }
            [Field("IntField", true)]
            public int intField { get; set; }
            [Field("VarcharField", true)]
            public string varcharField { get; set; }
            [Field("DatetimeField", true)]
            public DateTime datetimeField { get; set; }
            [Field("TimestampField", true)]
            public DateTime timestampField { get; set; }
            [Field("Boolfield", true)]
            public bool booleanField { get; set; }

        }


        [SetUp]
        public void Setup()
        {

        }
            #region select
        
            [Test]
            public void T01_select_with_string()
            {
                //select(string)
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                String sql = impl.Select("Tabella");
                Assert.IsTrue(sql == "select * from Tabella");





            ConnectorSqlServer con = new ConnectorSqlServer();
            con.Open();
            con.Execute(sql);
            Assert.Pass();
            // Assert.IsTrue(sql == "Non hai specificato una tabella");

            //ConnectorSqlServer con = new ConnectorSqlServer();

            /*con.Open();
            var dt = con.GetData(sql);
            Assert.IsTrue(dt.Rows.Count == 0);*/

        }




        [Test]
        public void T01_select_with_empty()
        {


            try
            {   //select(string)
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                String sql = impl.Select(string.Empty);
                Assert.IsTrue(sql == "select * from Tabella");
            } catch (Exception ex) { Assert.IsTrue(ex.Message == ""); }
          
            // Assert.IsTrue(sql == "Non hai specificato una tabella");

            //ConnectorSqlServer con = new ConnectorSqlServer();

            /*con.Open();
            var dt = con.GetData(sql);
            Assert.IsTrue(dt.Rows.Count == 0);*/

        }



        public void T02_select_with_string_and_DataFieldCondition()
            {
                //select(string,DataFieldCondition[])
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                var cond = new List<DataFieldCondition>();
                cond.Add(new DataFieldCondition("Persona", OperatorType.Equal, "Superman"));
                cond.Add(new DataFieldCondition("Eta", OperatorType.Major, 20));

                String sql = impl.Select("Tabella", cond.ToArray());
                Assert.IsTrue(sql == "select * from Tabella where Persona='Superman' AND Eta>20");
                
                //select * from Tabella
                //Type t = typeof(string);
                Assert.IsTrue(sql == "select * from Tabella;");

            }

            #endregion

            #region CopyTo

            [Test]
            public void T01_CopyTo()
            {
                TestORM o = new TestORM();
                TestORM o2 = new TestORM();
                o.id = 25;
                o.varcharField = "pippo";
                o.CopyTo(o2);



                Assert.IsTrue(o.id == o2.id);
                Assert.IsTrue(o.varcharField == o2.varcharField);
            }

            #endregion

            #region insert
            [Test]
            public void T01_Insert_with_DalBase()
            {
                TestORM o = new TestORM();
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                o.id = 25;
                o.varcharField = "Superman";
                string sql = impl.Insert(o);




                Assert.IsTrue(sql == "insert into Tabella (Id,VarcharField) values (25,'Superman');");
            }
        #endregion

        #region delete
        public void T01_Delete_with_DalBase()
            {
                TestORM o = new TestORM();
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                o.id = 25;

                string sql = impl.Delete(o);




                Assert.IsTrue(sql == "delete from Tabella where id=25;");
            }
        #endregion

        #region update
        public void T01_Update()
            {
                TestORM o = new TestORM();
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                o.id = 25;
                o.varcharField = "Superman";

                string sql = impl.Update(o);
                Assert.IsTrue(sql == "update Tabella set VarcharField=Superman where id=25;");
                Assert.IsTrue(sql == "update Tabella set Id=25,VarcharField=Superman where id=25;");
            }
        #endregion

        #region SelectCount
        public void T01_SelectCount()
            {
                //SelectCount(Type,DataFieldCondition[])
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                DataFieldCondition[] cond = new DataFieldCondition[1];
                cond[0].Name = "Persona";
                cond[0].Value = "Superman";
                cond[0].Operator = OperatorType.Equal;
                cond[1].Name = "Eta";
                cond[1].Value = 20;
                cond[1].Operator = OperatorType.Major;
                String sql = impl.SelectCount(typeof(TestORM), cond);
                Assert.IsTrue(sql == "select count(*) from Tabella where Persona='Superman' AND Eta>20;");
            }
            [Test]
            public void T02_SelectCount_with_Type()
            {
                //SelectCount(Type)
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                String sql = impl.SelectCount(typeof(TestORM));
                Assert.IsTrue(sql == "select count(*) from Tabella;");
            }
            [Test]
            public void T03_SelectCount_with_String_and_DataFieldCondition()
            {
                //SelectCount(string,DataFieldCondition[])
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                DataFieldCondition[] cond = new DataFieldCondition[1];
                cond[0].Name = "Persona";
                cond[0].Value = "Superman";
                cond[0].Operator = OperatorType.Equal;
                cond[1].Name = "Eta";
                cond[1].Value = 20;
                cond[1].Operator = OperatorType.Major;
                String sql = impl.SelectCount("Tabella", cond);
                Assert.IsTrue(sql == "select count(*) from Tabella where Persona='Superman' AND Eta>20;");
            }
        #endregion

        #region insert bulk
        public void T01_InsertBulk()
            {
                //InsertBulk(DalBase[])
                CommandDALParserSqlServer impl = new CommandDALParserSqlServer();
                TestORM[] dalB = new TestORM[2];
                dalB[0].varcharField = "Superman";
                dalB[1].varcharField = "Batman";
                dalB[0].intField = 20;
                dalB[1].intField = 30;
                string sql = impl.InsertBulk(dalB);
                Assert.IsTrue(sql == "insert into Tabella (IntField,VarcharField) values (20,'Superman'),(30,'Batman');");


            }
            #endregion
        }

    
}