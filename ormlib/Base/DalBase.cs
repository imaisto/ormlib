﻿using orm_lib.Connector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Text;
using System.Windows.Input;
using static orm_lib.Base.Attributes;

namespace orm_lib.Base
{
    public abstract class DalBase
    {


        #region property


        public long idbase {
            get {

                return (long)this.GetValue(this.IDKey);
            }
        
        }


        /// <summary>
        /// Ritorna il nome del campo con l'attributo IDKey
        /// </summary>
        public string IDKey { 
            get {
                //to do
                return "";
            } 
        }



        /// <summary>
        /// Ritorna i campi con l'attributo Key
        /// </summary>
        public string[] Key
        {
            get
            {
                //to do
                return new string[] { };
            }
        }

        /// <summary>
        /// restituisce il nome della tabella definito
        /// nel l'attributo di classe Table
        /// </summary>
        public string Table
        {
            get
            {   //tirare fuori il nome dell'atributo table
                //to do
                Type t = this.GetType();
                Table tab = (Table)Attribute.GetCustomAttribute(t, typeof(Table));
                return tab.Name;
            }
        }


        public IConnector Connection { get; set; }




        #endregion


        #region method


        /// <summary>
        /// Restituisce una lista di DalBaseFields
        /// </summary>
        /// <returns></returns>
        public ListDataField GetDataFields() {
            //todo
            return null;
        
        }


        #region Get\Set\Value

        /// <summary>
        /// Ritorna il valore della propietà con
        /// l'atributo [Field("Name")] passato
        /// </summary>
        /// <param name="FieldName"></param>
        /// <returns></returns>
        public object GetValue(string FieldName) {
            //todo
            Type t = this.GetType();
            PropertyInfo[] properties=t.GetProperties();
            foreach(PropertyInfo p in properties)
            {
                Field f=(Field) Attribute.GetCustomAttribute(p, typeof(Field));
                if(f.Name == FieldName)
                {
                    return p.GetValue(this);
                }
                else if(p.Name == FieldName)
                {
                    return p.GetValue(this);
                }
            }
            
            return null;
        }

        public object SetValue(string FieldName, object value)
        {
            Type t = this.GetType();
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo p in properties)
            {
                Field f = (Field)Attribute.GetCustomAttribute(p, typeof(Field));
                if (f.Name == FieldName)
                {
                    p.SetValue(this, value);
                    return p;
                }
                else if (p.Name == FieldName)
                {
                    p.SetValue(this, value);
                    return p;
                }
            }

            return null;
            
        }



        #endregion


        #region load

        public void Load(int id) {
            
        }

        public void Load(DataRow row)
        {
            //caricare me stesso riga di datatable
            //row.Table.Columns[0].ColumnName;//se uguale alla proprieta o attributo , mettere la cella nella proprieta
            //Todo 
            Type t = this.GetType();
            foreach(DataColumn col in row.Table.Columns)
            {
                object value = row[col];
                string colName = col.ToString();
                PropertyInfo[] props = t.GetProperties();
                foreach(PropertyInfo prop in props)
                {
                    Field f = (Field)Attribute.GetCustomAttribute(prop, typeof(Field));
                    if (f.Name==colName)
                    {
                        prop.SetValue(value, this);
                        break;
                    }
                    if(prop.Name==colName)
                    {
                        prop.SetValue(value, this);
                        break;
                    }
                }
            }
        }

        public void Load(ICommand cmd) {
            //Todo 
        }

        public void Load(DataFieldCondition Field)
        {
            //Todo 
        }

        public void Load(DataFieldCondition[] Field)
        {
            //Todo 
        }

        #endregion

        #region loadList

        public IList LoadList(string Command)
        {
            return null;
        }

        public IList LoadList(params DataFieldCondition[] Field)
        {
            return null;
        }


        public IList LoadList()
        {
            return null;
        }


        public IList LoadList(DataTable dt)
        {
            return null;
        }


        #endregion



        #region IList LoadListPage

        /// <summary>
        /// restituisce la lista degli elemnti
        /// </summary>
        /// <param name="page_size">dimensione della pagina</param>
        /// <param name="index_page">indice della pagina</param>
        /// <param name="Fields">campi per filtrare</param>
        /// <returns></returns>
        public IList LoadListPage(int page_size, int index_page ,params DataFieldCondition[] Fields)
        {
            return null;
        }


        #endregion


        #region Count

        public long Count(string Command)
        {
            return 0;
        }

        public long Count(params DataFieldCondition[] Field)
        {
            return 0;
        }


        public long Count()
        {
            return 0;
        }


        public long Count(DataTable dt)
        {
            return 0;
        }


        #endregion


        #region Exist

        public bool Exist(int id)
        {
            //Todo 
            return false;
        }
        public bool Exist(ICommand cmd)
        {
            //Todo 
            return false;
        }

        public bool Exist(DataFieldCondition Field)
        {
            //Todo 
            return false;
        }

        public bool Exist(DataFieldCondition[] Field)
        {
            //Todo 
            return false;
        }

        #endregion

        #region Insert\update\save

        public void Insert()
        {
            //Todo 
        }

        public void Update()
        {
            //Todo 
        }

        

        public void Save()
        {
            if (this.idbase == 0)
                this.Insert();
            else
                this.Update();
        }

        public void Delete()
        {
            //Todo 
        }
        public void CopyTo(DalBase o)
        {
            //devo vedere se anche la tabella coincide?????
            
            if(o.GetType()==this.GetType())
            {   
                PropertyInfo[] props=this.GetType().GetProperties();
                foreach(PropertyInfo pi in props)
                {   
                    //field and value
                    Field f=(Field) Attribute.GetCustomAttribute(pi, typeof(Field));
                    object value=pi.GetValue(this);
                    if (o.SetValue(f.Name, value)!=null)
                    {

                    }
                    else
                    {
                        o.SetValue(pi.Name, value);
                    }
                }
            }
           
        }
        #endregion 
        #endregion 

    }
}
