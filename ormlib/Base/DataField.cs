﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace orm_lib.Base
{

    public enum OperatorType { 
        [Description("<>")]
        None =0,
        [Description("=")]
        Equal = 1,
        [Description(">")]
        Major =2,
        [Description("<")]
        Minor =3,
        [Description(">=")]
        MajorEqual =4,
        [Description("<=")]
        MinorEqual =5,
        [Description("IN")]
        In =6,
        [Description("NOT IN")]
        NotIn =7,
        [Description("AND")]  ///togliere!!
        And =8,
        [Description("OR")] ///togliere!!
        Or = 9,
        [Description("NOT")] ///togliere!!
        Not = 10,
        [Description("EXISTS")] ///togliere!!
        Exists = 11,
        [Description("NOT EXISTS")] ///togliere!!
        NotExists = 12,
        [Description("ALL")] ///togliere!!
        All = 13,
        [Description("ANY")] ///togliere!!
        Any = 14,
        [Description("MAX")] ///togliere!!
        Max = 15,
        [Description("MIN")] ///togliere!!
        Min = 16,
        [Description("AS")] ///togliere!!
        As = 17,
        [Description("Count")] ///togliere!!
        Count = 18,
        [Description("Distinct")] ///togliere!!
        Distinct = 19,
        [Description("SUM")] ///togliere!!
        Sum = 20,
        [Description("GROUP BY")] ///togliere!!
        GroupBy = 21,
        [Description("+")] ///togliere!!
        Plus = 22,
        [Description("-")] ///togliere!!
        Minus = 23,
        [Description("*")] ///togliere!!
        Multiply = 24,
        [Description("/")] ///togliere!!
        Divide = 25,
        [Description("%")] ///togliere!!
        Modulo = 26,
        [Description("BETWEEN")] ///togliere!!
        Between = 27,
        [Description("SOME")] ///togliere!!
        Some = 28,
        [Description("LIKE")] ///togliere!!
        Like = 29,
        [Description("HAVING")] ///togliere!!
        Having = 30,


        

        //To do
    }


    public class DataField
    {

        public string Name{ get; set; }
        public object Value { get; set; }


        public Type TypeValue {
            get {
                if (this.Value == null) return null;
                if (this.Value.GetType() == typeof(System.DBNull)) return null;
                return this.Value.GetType();
            } 
        }

    }



    public class DataFieldCondition : DataField
    {
        
        public DataFieldCondition(string name, OperatorType op,object value) {
            
            base.Name = name;
            this.Operator = op;
            base.Value = value;
        }
        public DataFieldCondition(string name,  object value)
        {

            base.Name = name;
            this.Operator = OperatorType.Equal;
            base.Value = value;
        }

        public OperatorType Operator { get; set; }
        

        public string getOperator()
        {
            Type t = Operator.GetType();
            MemberInfo[] memberInfo = t.GetMember(Operator.ToString());
            object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
            return ((DescriptionAttribute)attrs[0]).Description;
        }
    }

}
