﻿using System;
using System.Collections.Generic;
using System.Text;

namespace orm_lib.Base
{
    public class Attributes
    {


        public class Table : Attribute
        {


            public Table()
            {
            }

            public Table(string Name)
            {
                this.Name = Name;
            }


            public string Name { get; set; }


        }




        public class Field : Attribute {


            public Field()
            {
            }

            public Field(string Name)
            {
                this.Name = Name;
                this.Unique = Unique;
            }

            public Field(string Name, bool Unique)
            {
                this.Name = Name;
                this.Unique = Unique;
            }

            public string Name { get; set; }
            public bool Unique { get; set; }
        
        }




        public class IDKey : Field
        {
            public IDKey(string Name)
            {
                base.Name = Name;
                base.Unique = true;
            }

            public IDKey()
            {
                base.Unique = true;
            }

        }


        public class Key : Field
        {
            public Key(string Name) {
                base.Name = Name;
            }

            public Key() {
                base.Unique = true;
            }

        }


    }
}
