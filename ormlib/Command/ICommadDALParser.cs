﻿using orm_lib.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace orm_lib.Command
{
    interface ICommadDALParser
    {

        /// <summary>
        /// restituisce una stringa del tipo
        /// select count(*) from [TableName]
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        string SelectCount(string TableName);


        string SelectCount(string TableName, DataFieldCondition[] FieldWhere);

        string SelectCount(Type T, DataFieldCondition[] FieldWhere);
        string SelectCount(Type T);


        /// <summary>
        /// nel parametro passo passare il nome della tabella o cun select complessa
        /// nel primo caso verrà creata
        /// select * from [tabella] 
        /// nel secondo caso
        /// select * from ([select esempio from esempio innerjoin esempuio2 where esempio not in ('test1')]) 
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        string Select(string TableName);

        /// <summary>
        /// nel parametro passo passare il nome della tabella o cun select complessa
        /// nel primo caso verrà creata
        /// select * from [tabella] where [DataFieldCondition.Name = DataFieldCondition.value]
        /// nel secondo caso
        /// select * from ([select esempio from esempio innerjoin esempuio2 where esempio not in ('test1')]) where [DataFieldCondition.Name = DataFieldCondition.value]
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        string Select(string TableName, DataFieldCondition[] FieldWhere);

        /// <summary>
        /// Crea un comando di select con tutti i campi filtrato per la condizione
        /// </summary>
        /// <param name="FieldWhere"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        string Select(Type T, DataFieldCondition[] FieldWhere);
        string Select(Type T);

        string Insert(DalBase o);




        /// <summary>
        /// Crea un comando di InsertBulk
        /// del Tipo
        /// Insert into [Table] (ferlds ....) valuese (primi dati....),(secondi Dati)
        /// </summary>
        /// <param name="FieldWhere"></param>
        /// <param name="T"></param>
        /// <returns></returns>
        string InsertBulk(DalBase[] list);


        string Update(DalBase o);

        string Delete(DalBase o);

    }
}
