﻿using System;
using System.Collections.Generic;
using System.Text;
using orm_lib.Base;
using orm_lib.Command;
using System.Reflection;
using static orm_lib.Base.Attributes;

namespace orm_lib.Command
{
    public class CommandDALParserSqlServer : ICommadDALParser
    {

        /// <summary>
        /// nel parametro passo passare il nome della tabella o cun select complessa
        /// nel primo caso verrà creata
        /// select * from [tabella] 
        /// nel secondo caso
        /// select * from ([select esempio from esempio innerjoin esempuio2 where esempio not in ('test1')]) 
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>
        public string Select(string TableName) {
            //todo
            if(string.IsNullOrWhiteSpace(TableName))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if(TableName.Trim().ToLower().Contains("select"))
            {
                return $@"select * from ({TableName})";
            }
            

            return $@"select * from {TableName}";
            
        }

        /// <summary>
        /// nel parametro passo passare il nome della tabella o cun select complessa
        /// nel primo caso verrà creata
        /// select * from [tabella] where [DataFieldCondition.Name = DataFieldCondition.value]
        /// nel secondo caso
        /// select * from ([select esempio from esempio innerjoin esempuio2 where esempio not in ('test1')]) where [DataFieldCondition.Name = DataFieldCondition.value]
        /// </summary>
        /// <param name="TableName"></param>
        /// <returns></returns>

        public string Select(string TableName, DataFieldCondition[] FieldWhere) {
            //todo
            String query =String.Empty;
           
            if (string.IsNullOrWhiteSpace(TableName))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (TableName.Trim().ToLower().StartsWith("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }


            foreach (DataFieldCondition condition in FieldWhere)
            {

                query += $@"{condition.Name} {condition.getOperator()} ";
                //query = query + condition.Name+" "+condition.getOperator();
                Type tipe = condition.Value.GetType();
                if (tipe == typeof(string))
                {
                    string str = $@"'{condition.Value}' AND";
                    query += str;
                }
                else
                {
                    query += $@"{condition.Value} AND";
                }




            }
            query.Remove(query.Length - 4, 3);
            return $@"select * from {TableName} where {query}";
        }


        public string Select(DataFieldCondition[] FieldWhere, Type T)
        {
            
            
            String query = string.Empty;
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (string.IsNullOrWhiteSpace(t.Name))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }

            foreach (DataFieldCondition condition in FieldWhere)
            {

                query += $@"{condition.Name} {condition.getOperator()} ";
                //query = query + condition.Name+" "+condition.getOperator();
                Type tipe = condition.Value.GetType();
                if (tipe == typeof(string))
                {
                    string str = $@"'{condition.Value}' AND";
                    query += str;
                }
                else
                {
                    query += $@"{condition.Value} AND";
                }




            }
            query.Remove(query.Length - 4, 3);
            return $@"select * from {t.Name} where {query}";
        }
        public string Select(Type T)
        {
            if (T != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (string.IsNullOrWhiteSpace(t.Name))
            {
                throw new Exception("Non hai specificato una tabella, è neccessario specificare il nome della tabella tramite l'attributo table");
            }
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }



            return this.Select(T.Name);
        }
        
        public string Delete(DalBase o)
        {   
            
            if (findKey(o) == null)
            {
                throw new Exception($@"Chiave non trovata nella classe {o.GetType().Name}, è neccessario definire un atributo chiave non essistente");
            }
            Type T = o.GetType();
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            string query = $@"delete from {t.Name} where id={findKey(o)}";
            return query;
        }
        //gestire le eccezioni
        //finire i select
        //truncate di tables

        public string Insert(DalBase o)
        {
            if (o.GetType() != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            if (findKey(o) == null)
            {
                throw new Exception($@"Chiave non trovata nella classe {o.GetType().Name}, è neccessario definire un atributo chiave non essistente");
            }
            Type T = o.GetType();

            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            PropertyInfo[] properties = T.GetProperties();
            bool isAuto = false;

            foreach (PropertyInfo prop in properties)
            {
                if (Attribute.GetCustomAttribute(prop, typeof(IDKey)) != null)
                {
                    isAuto = true;
                    break;
                }

            }
            string query = $@"insert into {t.Name} (";

            if (!isAuto)
            {

                foreach (PropertyInfo property in properties)
                {
                    Field f = (Field)Attribute.GetCustomAttribute(property, typeof(Field));
                    query += $@"{f.Name},";
                }
                query.Remove(query.Length - 1, 1);
                query += ") values (";
                properties = o.GetType().GetProperties();
                foreach (PropertyInfo property in properties)
                {
                    if (property.PropertyType == typeof(string))
                    {
                        query += $@"'{property.GetValue(o)}',";
                    }
                    else
                    {
                        query += $@"{property.GetValue(o)},";
                    }
                }
                query.Remove(query.Length - 1, 1);
                query += ");";

            }
            else
            {
                for (int i = 1; i < properties.Length; i++)
                {
                    Field f = (Field)Attribute.GetCustomAttribute(properties[i], typeof(Field));
                    query += $@"{f.Name},";
                }
                query.Remove(query.Length - 1, 1);
                query += ") values (";
                properties = o.GetType().GetProperties();
                for (int i = 1; i < properties.Length; i++)
                {
                    if (properties[i].PropertyType == typeof(string))
                    {
                        query += $@"'{properties[i].GetValue(o)}',";
                    }
                    else
                    {
                        query += $@"{properties[i].GetValue(o)},";
                    }
                }
                query.Remove(query.Length - 1, 1);
                query += ");";
            }
            return query;

        }



        public string Update(DalBase o)
        {  
            if (o.GetType() != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            if (findKey(o) == null)
            {
                throw new Exception($@"Chiave non trovata nella classe {o.GetType().Name}, è neccessario definire un atributo chiave non essistente");
            }
            
            Type T = o.GetType();
            PropertyInfo[] properties = T.GetProperties();
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            string query = $@"update {t.Name} set ";
            //update tabella set colonna=valore;
            foreach (PropertyInfo property in properties)
            {
                Field f = (Field)Attribute.GetCustomAttribute(property, typeof(Field));
                query += $@"{f.Name}={property.GetValue(o)},";
            }
            query.Remove(query.Length - 1, 1);

            query += $@"where id={findKey(o)};";

            return query;
        }

        public string SelectCount(string TableName)
        {
            if (string.IsNullOrWhiteSpace(TableName))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if(TableName.GetType()!=typeof(string))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            if (TableName.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }

            return $@"select count(*) from {TableName}";

        }

        public string SelectCount(string TableName, DataFieldCondition[] FieldWhere)
        {
            if (string.IsNullOrWhiteSpace(TableName))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (TableName.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            if(TableName.GetType()==typeof(string))
            {
                    throw new Exception("Il valore inserito non può esssere accettato");
            }
            string query = string.Empty;
            foreach (DataFieldCondition condition in FieldWhere)
            {

                query += $@"{condition.Name} {condition.getOperator()} ";
                //query = query + condition.Name+" "+condition.getOperator();
                Type tipe = condition.Value.GetType();
                if (tipe == typeof(string))
                {
                    string str = $@"'{condition.Value}' AND";
                    query += str;
                }
                else
                {
                    query += $@"{condition.Value} AND";
                }




            }
            query.Remove(query.Length - 4, 3);
            return $@"select count(*) from {TableName} where {query}";
        }
        
        public string SelectCount(Type T, DataFieldCondition[] FieldWhere)
        {
            String query = string.Empty;
            if (T != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            if (FieldWhere.GetType() != typeof(DataField))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            if (string.IsNullOrWhiteSpace(t.Name))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }

            foreach (DataFieldCondition condition in FieldWhere)
            {

                query += $@"{condition.Name} {condition.getOperator()} ";
                //query = query + condition.Name+" "+condition.getOperator();
                Type tipe = condition.Value.GetType();
                if (tipe == typeof(string))
                {
                    string str = $@"'{condition.Value}' AND";
                    query += str;
                }
                else
                {
                    query += $@"{condition.Value} AND";
                }




            }
            query.Remove(query.Length - 4, 3);
            return $@"select count(*) from {t.Name} where {query}";
        }

        public string SelectCount(Type T)
        {
            if (T != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }
            Table t = (Table)Attribute.GetCustomAttribute(T, typeof(Table));
            
            if (string.IsNullOrWhiteSpace(t.Name))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (t.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            return $@"select count(*) from {t.Name}";
        }

        

        public string InsertBulk(DalBase[] list)
        {
            Table tab = (Table)Attribute.GetCustomAttribute(list[0].GetType(), typeof(Table));
            if (string.IsNullOrWhiteSpace(tab.Name))
            {
                throw new Exception("Non hai specificato una tabella");
            }
            if (tab.Name.Contains("select"))
            {
                return "select * from ([select esempio from esempio innerjoin esempio2 where esempio not in ('test1')])";
            }
            if(list.GetType()!=typeof(DalBase))
            {
                    throw new Exception("Il valore inserito non può esssere accettato");
            }
            string values = string.Empty;
            string tables = "(";
            PropertyInfo[] props = list[0].GetType().GetProperties();
            foreach (PropertyInfo prop in props)
            {   //aggiunto prop.GetValue caso mai il valore non c'e di non aggiungere il field, il field ed il valore null
                
                if(Attribute.GetCustomAttribute(prop, typeof(IDKey)) != null && prop.GetValue(list[0])!=null)
                {   
                    Field field = (Field)Attribute.GetCustomAttribute(prop,typeof(Field));
                    tables += $@"{field.Name},";
                }
            }
            tables.Remove(tables.Length -1,1);
            tables += ")";
            foreach(DalBase item in list)
            {
                values += "(";
                PropertyInfo[] valuesProps=item.GetType().GetProperties();
                foreach(PropertyInfo prop in valuesProps)
                {   
                    if (prop.PropertyType == typeof(string))
                    {
                        values += $@"'{prop.GetValue(item)}',";
                    }
                    else
                    {
                        values += $@"'{prop.GetValue(item)}',";
                    }
                }
                values.Remove(valuesProps.Length -1,1);
                values += "),";//togliere virgola
            }
            values.Remove(values.Length-1,1);
            return $@"insert into {tab.Name} {tables} values {values};";
        }

        public object findKey(DalBase d)
        {
            if (d.GetType() != typeof(DalBase))
            {
                throw new Exception("Il valore inserito non può esssere accettato");
            }

            PropertyInfo[] props = d.GetType().GetProperties();
            foreach (PropertyInfo property in props)
            {
                if (Attribute.GetCustomAttribute(property, typeof(IDKey)) != null)
                {
                    IDKey keyID = (IDKey)Attribute.GetCustomAttribute(property, typeof(IDKey));
                    return keyID.Name;
                }
                else if (Attribute.GetCustomAttribute(property, typeof(Key)) != null)
                {
                    Key key = (Key)Attribute.GetCustomAttribute(property, typeof(Key));
                    return key.Name;
                }
            }

            return null;
        }

        public string Select(Type T, DataFieldCondition[] FieldWhere)
        {
            throw new NotImplementedException();
        }
    }
}
