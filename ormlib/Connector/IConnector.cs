﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace orm_lib.Connector
{
    public interface IConnector
    {

        void Open();
        void Close();
        DataTable GetData(string sql);
        void Execute(string sql);
    }
}
