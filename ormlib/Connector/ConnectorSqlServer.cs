﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace orm_lib.Connector
{
    
    public class ConnectorSqlServer : IConnector
    {
        


        public String ConnectionString= "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\sorin.miauca\\source\\repos\\TestThings\\TestThings\\Database1.mdf;Integrated Security=True";
        public System.Data.IDbConnection Connection;
        public void Close()
        {
            
            try
            {
                Connection.Close();
            }
            catch(NullReferenceException e)
            {
                throw new Exception("La connessione non può essere chiusa");
            }
            

        }

        public void Open()
        {
            
            Connection = new System.Data.SqlClient.SqlConnection(ConnectionString);
            try
            {
                Connection.Open();
            }
            catch(SqlException e)
            {
                
                throw new Exception("Connessione non riuscita");
            }
           


        }


        /// <summary>
        /// Metodo che prende un stringa sql, visto che deve ritornare dei dati si pressupone sia una select, e ritorna un DataTable come oggetto
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        
        //Da testare ancora
        public DataTable GetData(string sql)
        {
            string[] array = sql.Split(' ');
            DataTable table = new DataTable(array[3]);

            this.Open();
            IDbCommand command = Connection.CreateCommand();
            command.CommandText = sql;
            IDataReader reader = command.ExecuteReader();
            //reader.GetName(0)
            //reader[0] = valore
            //reader[0].getType() = tipo

            //vedere come gestire se mi da null, perche poi il GetType sara DBNull
            while (reader.Read())
            {

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    DataColumn col = new DataColumn();
                    col.DataType = reader[i].GetType();
                    col.ColumnName = reader.GetName(i);
                    table.Columns.Add(col);
                }
                break;
            }
            reader.Close();
            reader = command.ExecuteReader();
            while (reader.Read())
            {
                DataRow row = table.NewRow();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row[reader.GetName(i)] = reader[i];
                }
                table.Rows.Add(row);
            }

                return table;
        }
        /// <summary>
        /// Metodo che prende una stringa sql, e si pressupone faccio una insert,delete,o update perchè non ritorna dati
        /// </summary>
        /// <param name="sql"></param>
        public void Execute(string sql)
        {
            if(string.IsNullOrWhiteSpace(sql) || sql.Trim().ToLower().StartsWith("select"))
            {
                throw new Exception("La richiesta non è valida");
            }

            this.Open();
            IDbCommand command = Connection.CreateCommand();
            command.CommandText = sql;
            try
            {
                command.ExecuteNonQuery();
            }
            catch(SqlException e)
            {
                throw new Exception("La richesta non è valida");
            }
            
            
            this.Close();
        }
        //finire getData, Execute, mettere la stringa di connessione giusta e fare i test

        


    }
    
}
